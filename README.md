# [files.oracledba.net](https://files.oracledba.net) source codes

<br/>

### Run files.oracledba.net on localhost

    # vi /etc/systemd/system/files.oracledba.net.service

Insert code from files.oracledba.net.service

    # systemctl enable files.oracledba.net.service
    # systemctl start files.oracledba.net.service
    # systemctl status files.oracledba.net.service

http://localhost:4069
